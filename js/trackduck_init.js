/**
 * @file
 * Javascript behaviors for the TrackDuck module.
 */

(function ($) {
    Drupal.behaviors.trackduckInit = {

        attach: function() {
            var TrackDuck = {};

            TrackDuck.getStatus = function(withInterval) {
                var trackduckSettings = Drupal.settings.trackduckSettings;
                var url = 'https://app.trackduck.com/api/bar/settings/?url=' + encodeURIComponent(trackduckSettings.url);
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'GET',
                    beforeSend: function(xhr){
                        xhr.withCredentials = true
                    },
                    xhrFields : {
                        withCredentials: true
                    },
                    crossDomain: true,
                    success: function(xhr){
                        //if project created, clear interval
                        if (withInterval == true) {
                            clearInterval(TrackDuck.checkStatus);
                        }

                        //if response is positive, save settings
                        if (trackduckSettings.status == 3 && trackduckSettings.id == "") {
                            $('#trackduck-active').val(1);
                            $('#trackduck-id').val(xhr.projectId);
                            $("#edit-submit").trigger("click");
                        //show disable container if integration enabled
                        } else if (trackduckSettings.status == 1 && trackduckSettings.id != "") {
                            $('#td-disable-container').show();
                            navigator.sayswho = (function(){
                                var ua = navigator.userAgent,
                                    N = navigator.appName, tem,
                                    M = ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*([\d\.]+)/i) || [];
                                M = M[2] ? [M[1], M[2]] :[N, navigator.appVersion, '-?'];
                                return M[0];
                            })();
                            if (navigator.sayswho == "Chrome") {
                                $('#trackduck-firefox').hide();
                            }
                            if (navigator.sayswho == "Firefox") {
                                $('#trackduck-chrome').hide();
                            }
                        //show enable container if integration disabled
                        } else if (trackduckSettings.status == 2 && trackduckSettings.id != "") {
                            $('#td-enable-container').show();
                        }
                        //if project id differs, set the new one
                        if (trackduckSettings.id != xhr.projectId) {
                            $('#trackduck-id').val(xhr.projectId);
                            $("#edit-submit").trigger("click");
                        }

                    },
                    error: function(xhr) {
                        //if not logged in
                        if (xhr.status == 401) {
                            $('#td-login-container').show();
                        }
                        //if logged in
                        else if (xhr.status == 403) {
                            //submit settings after logging in
                            if (trackduckSettings.status == 4) {
                                $('#trackduck-active').val(3);
                                $("#edit-submit").trigger("click");
                            //if logged in but there is no project created
                            } else if (trackduckSettings.status == 3 && trackduckSettings.id == "") {
                                $('#td-enable-container').show();
                            //if logged in but project is deleted or trying to access other account's project
                            } else {
                                $('#trackduck-active').val(3);
                                $('#trackduck-id').val("");
                                $("#edit-submit").trigger("click");
                            }

                        }
                    }
                });
            };

            TrackDuck.getStatus(false);

            $("#trackduck-enable").click(function(e){
                var trackduckSettings = Drupal.settings.trackduckSettings;
                //if disabled and has id, enable
                if (trackduckSettings.status == 2 && trackduckSettings.id != "") {
                    $('#trackduck-active').val(1);
                    $("#edit-submit").trigger("click");
                    e.preventDefault();
                //if has no id, set interval and wait for id
                } else if (trackduckSettings.status == 3 && trackduckSettings.id == ""){
                    TrackDuck.checkStatus = setInterval(function(){TrackDuck.getStatus(true)},5000);
                }
            });

            $('#trackduck-disable').click(function(e){
                $('#trackduck-active').val(2);
                $("#edit-submit").trigger("click");
                e.preventDefault();
            });
        }
    }
})(jQuery);
