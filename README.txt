TrackDuck Module Readme
=======================


Installation
============

To install this module, place it in your sites/all/modules folder and enable it
on the modules page.


Module configuration
====================

After enabling module go to project settings page (admin/config/development/trackduck)
and signup for TrackDuck using your Google, Facebook account or email service.
You will be redirected to TrackDuck and after successful signup you will be redirected
back to your Drupal website. After that you should see Enable button, so click on it.
After clicking enable, new browser tab will open. You will see your website URL there,
click on "+" on the left of URL field and thats it! Integration is enabled when you see
Disable button on the project settings page (admin/config/development/trackduck).
Visit your homepage and add your first feedback by using TrackDuck Toolbar!


Plugin configuration
====================

You can configure TrackDuck toolbar and feedback button at project settings page
by clicking "Open project settings" or going directly to https://app.trackduck.com and
selecting your Drupal project.
